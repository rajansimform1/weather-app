const weatherSearch = document.querySelector("#weatherForm");
const searchLocation = document.querySelector("#location");
const cityname = document.getElementById("cityname");
const forcastdata = document.getElementById("forcastdata");

weatherSearch.addEventListener("submit", async (e) => {
  e.preventDefault();
  const address = searchLocation.value;
  console.log(address);

  // fetch(`http://localhost:5000/weather?address=${address}`).then((res) => {
  //   res.json().then((data) => {
  //     console.log(data);
  //   });
  // });

  cityname.innerHTML = `loading...`;

  const res = await fetch(`/weather?address=${address}`);
  const data = await res.json();
  //console.log(data);
  if (data.error) {
    alert(data.error);
    cityname.innerHTML = "";
    forcastdata.innerHTML = "";
  } else {
    cityname.innerHTML = `City Name : <b>${data.name}</b>`;
    forcastdata.innerHTML = `<table cellpadding="10px" border="1px" align="center">
      <tr>
        <th>Parameter</th>
        <th>Details</th>
      </tr>
      <tr>
        <td>Weather</td>
        <td>${data.weather[0].description}</td>
      </tr>
      <tr>
        <td>Temp</td>
        <td>${data.main.temp} ºC</td>
      </tr>
      <tr>
        <td>Humidity</td>
        <td>${data.main.humidity}%</td>
      </tr>
      <tr>
        <td>Wind</td>
        <td>${data.wind.speed} m/s , ${data.wind.deg}º</td>
      </tr>
    </table>`;
  }
});
