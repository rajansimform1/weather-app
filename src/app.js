import express from "express";
import hbs from "hbs";
import { dirname, join } from "path";
import { fileURLToPath } from "url";
import { forcast } from "./utils/forcast.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const author = "Rajan Lodhiya";
const app = express();
const port = process.env.PORT || 5000;

//static folder
app.use(express.static(join(__dirname, "../public")));

//partial
const partialPath = join(__dirname, "../templates/partials");

app.set("view engine", "hbs");
app.set("views", join(__dirname, "../templates"));
hbs.registerPartials(partialPath);

app.get("", (req, res) => {
  res.render("index", {
    title: "Welcome to Weather App",
    author,
  });
});

app.get("/weather", (req, res) => {
  const address = req.query.address;

  // check if address is given in query parameter
  if (!address) {
    return res.send({
      error: "please enter address",
    });
  }

  //if address is given then call the forcast function
  forcast(address, (err, data) => {
    if (err) {
      res.send({
        error: err,
      });
    }
    res.send(data);
  });
});

app.get("/about", (req, res) => {
  res.render("about", {
    title: "About Page",
    author,
  });
});

app.get("*", (req, res) => {
  res.render("404", {
    title: "Error 404",
    errMsg: "Page Not Found...",
    author,
  });
});

// //home page
// app.get("", (req, res) => {
//   res.send("Home Page");
// });

// //about page
// app.get("/about", (req, res) => {
//   res.send("About Page");
// });

// //help page
// app.get("/about/help", (req, res) => {
//   res.send("Help Page");
// });

app.listen(port, () => {
  console.log(`server started on port ${port}...`);
});
